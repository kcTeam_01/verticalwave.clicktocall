﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATSIViewpointAddins.Contract
{
    [Serializable]
    public class CustomDataEntry
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    [Serializable]
    public class CallInfoEntry
    {
        public string CallerIdName { get; set; }
        public string CallerIdNumber { get; set; }

        public List<CustomDataEntry> CustomData { get; set; }

        public string CallLogId { get; set; }

        public bool AllowAnswer { get; set; }

        public bool AllowDisconnect { get; set; }
    }

    [Serializable]
    public class IPC_PopperToAddin
    {
        public string CommandName { get; set; }
        public string CallID { get; set; }
        public string CommandArgument { get; set; }
        public List<CustomDataEntry> CustomData { get; set; }
    }

    [Serializable]
    public class IPC_AddinToPopper 
    {
        public IPC_AddinToPopper()
        {
            this.Command = CommandType.NotSet;
            this.Info = new CallInfoEntry();
        }
        public enum CommandType
        {
            NotSet,
            ClientAddinConnected,
            CallChanged,
            CallRemoved,
        }
        public CommandType Command { get; set; }
        public CallInfoEntry Info { get; set; }

        public string CallID { get; set; }

        public string Status { get; set; }
        public string NotificationType { get; set; }
    }
}
