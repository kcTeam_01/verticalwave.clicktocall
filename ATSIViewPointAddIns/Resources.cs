﻿using System;
using System.Linq;

namespace ATSI.ViewPoint.AddIns
{
    internal static class Resources
    {
        internal static class AddIn
        {
            public static string Description = "Custom Add-In for Affiliated Transportation";
            public static string Name = "ATSI";
        }

        internal static class MenuItems
        {
            //public static ToolStripItem NewDefaultItem
            //{
            //    get
            //    {
            //        var tsi = new ToolStripItem();

            //        return tsi;
            //    }
            //}
        }
    }
}