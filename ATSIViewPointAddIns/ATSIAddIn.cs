﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ATSI.ViewPoint.AddIns.Contract;
using ATSIViewpointAddins.Contract;
using TheCodeKing.Net.Messaging;
using Vertical.Wave.ViewPoint;
using Vertical.Wave.ViewPoint.Api;
using Vertical.Wave.ViewPoint.Api.Calls;

namespace ATSI.ViewPoint.AddIns
{
    internal class ATSIAddIn : AddInBase
    {
        private String LogPath = "c:\\AddInLog.txt";
        public ATSIAddIn()
        {
            this.Logger = new System.IO.StreamWriter(this.LogPath, false)
            {
                AutoFlush = true,
            };

            this.Logger.WriteLine("Constructor()");

            // Create our listener instance
            this.listener = XDListener.CreateListener(XDTransportMode.WindowsMessaging);

            // Register channels to listen on
            this.listener.RegisterChannel("IPC_PopperToAddin");
            this.listener.MessageReceived += new XDListener.XDMessageHandler(listener_MessageReceived);

            this.broadcaster = TheCodeKing.Net.Messaging.XDBroadcast.CreateBroadcast(XDTransportMode.WindowsMessaging, false);
            System.Diagnostics.Process.Start("atsiphone://localhost?Command=AddinLaunched");
        }

        public delegate void AsyncDelegate();

        public override BuiltInFolder DefaultFolderType
        {
            get
            {
                throw new NotSupportedException("Extensions do not have a default folder");
            }
        }

        public override ItemType DefaultItemType
        {
            get
            {
                return ItemType.NoItem;
            }
        }

        public override ObjectType DefaultType
        {
            get
            {
                return ObjectType.None;
            }
        }

        public override string Description
        {
            get
            {
                return Resources.AddIn.Description;
            }
        }

        public override Guid Id
        {
            get
            {
                return new Guid("{e21b0f85-314c-4e2c-ab68-57c1de4fd02c");
            }
        }

        public override string Name
        {
            get
            {
                return Resources.AddIn.Name;
            }
        }

        public override string NewDefaultMenuItemName
        {
            get
            {
                return String.Empty;
            }
        }

        public override string ViewHelpTopicId
        {
            get
            {
                return String.Empty;
            }
        }

        public override void AppendNewContextMenuItems(Explorer explorer, ToolStripItemCollection contextMenuItems)
        {
        }

        public override InspectorFormBase CreateInspector(IItem item, object parameter)
        {
            return null;
        }

        public override bool DisplayInspector(ItemType itemType, Folder currentFolder, out IItem item)
        {
            item = null;
            return false;
        }

        public override bool DisplayInspector(IItem item, object parameter)
        {
            return false;
        }

        public override Vertical.Wave.ViewPoint.Api.View GetDefaultView()
        {
            return null;
        }

        public override ItemViewControlBase GetNewViewControl(Vertical.Wave.ViewPoint.Api.View view, ObjectId defaultSelectedItemId)
        {
            return new ItemViewControlBase(view, 2, this, defaultSelectedItemId);
        }

        public override void InsertNewContextMenuDefaultItem(Explorer explorer, ToolStripItemCollection contextMenuItems)
        {
        }

        public override string ViewGoToMenuItemName(NavigationItem navigationItem)
        {
            return string.Empty;
        }

        public void WaitForVPToStart()
        {
            while (base.Application.Starting)
            {
                Delay(1000);
            }
            Delay(4000);
        }

        protected System.IO.StreamWriter Logger { get; set; }

        protected void Delay(int msToDelay)
        {
            DateTime now = DateTime.Now;
            TimeSpan timeSpan = DateTime.Now.Subtract(now);
            while (timeSpan.TotalSeconds * 1000.0 < (double)msToDelay)
            {
                System.Windows.Forms.Application.DoEvents();
                System.Threading.Thread.Sleep(5);
                timeSpan = DateTime.Now.Subtract(now);
            }
        }

        protected void DoneWaitingForVpToStart(IAsyncResult ar)
        {
            try
            {
                _session = base.Application.Session;
                if (_session.Status == SessionStatus.LoggedOn)
                {
                    _callCollection = _session.GetCallsInPersonalCallsFolder();
                    _callCollection.ItemAdded += new EventHandler<ItemCollectionNotifyEventArgs>(_callCollection_ItemChanged);
                    _callCollection.ItemChanged += new EventHandler<ItemCollectionNotifyEventArgs>(_callCollection_ItemChanged);
                    _callCollection.ItemRemoved += new EventHandler<ItemCollectionNotifyEventArgs>(_callCollection_ItemChanged);
                    _callCollection.Subscribe();

                    broadcaster.SendToChannel("IPC_AddinToPopper", new IPC_AddinToPopper()
                    {
                        Command = IPC_AddinToPopper.CommandType.ClientAddinConnected,
                    }.Serialize());
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLine("---------------------------------------------");
                this.Logger.WriteLine("Error: ");
                this.Logger.WriteLine(ex.Message);
                this.Logger.WriteLine(ex.StackTrace);
                this.Logger.WriteLine("---------------------------------------------");
            }
        }

        protected override void OnExplorerOpened(Explorer explorer)
        {
        }

        protected override void OnInitialize(Vertical.Wave.ViewPoint.Application application)
        {
            AsyncDelegate waitForStartup = new AsyncDelegate(this.WaitForVPToStart);
            waitForStartup.BeginInvoke(new AsyncCallback(this.DoneWaitingForVpToStart), waitForStartup);
        }

        protected override void OnUnload()
        {
            this.Logger.WriteLine("OnUnload()");
            _callCollection.ItemAdded -= _callCollection_ItemChanged;
            _callCollection.ItemChanged -= _callCollection_ItemChanged;
            _callCollection.ItemRemoved -= _callCollection_ItemChanged;

            this.Logger.Flush();
            this.Logger.Close();

            base.OnUnload();
        }

        private Vertical.Wave.ViewPoint.Api.Calls.CallItemViewCollection _callCollection;

        /// <summary>
        /// This is an instance of the session to the phone server.
        /// </summary>
        private Session _session;

        /// <summary>
        /// This is the interface for sending messages to the winform application.
        /// </summary>
        private IXDBroadcast broadcaster;

        /// <summary>
        /// This is the interface for receiving message from the winform application.
        /// </summary>
        private IXDListener listener;

        private void _callCollection_ItemChanged(object sender, ItemCollectionNotifyEventArgs e)
        {
            this.Logger.WriteLine("_callCollection_ItemChanged - Begin");
            try
            {
                this.Logger.WriteLine(e.ItemId.Id + " - " + e.NotificationType.ToString());
                CallItem call;
                var msg = new IPC_AddinToPopper()
                {
                    CallID = e.ItemId.ToString(),
                    NotificationType = e.NotificationType.ToString(),
                };

                if (_session.TryGetItem<CallItem>(e.ItemId, out call))
                {
                    this.Logger.WriteLine("Get Item Success");
                    msg.Status = call.Status.ToString();

                    if (call.Number.Length >= 7)
                    {
                        List<CustomDataEntry> CustomData = new List<CustomDataEntry>();
                        if (call.CustomData != null)
                        {
                            var tmp2 = call.CustomData.Select(p => new CustomDataEntry() { Name = p.Key, Value = p.Value }).ToList();
                            CustomData.AddRange(tmp2);
                        }

                        msg.Command = IPC_AddinToPopper.CommandType.CallChanged;
                        msg.Info = new CallInfoEntry()
                        {
                            CallerIdName = call.CallerIdName,
                            CallerIdNumber = call.CallerIdNumber,
                            CallLogId = call.CallLogId.Id,
                            CustomData = CustomData,
                            AllowAnswer = call.Features.AllowAnswer,
                            AllowDisconnect = call.Features.AllowDisconnect,
                        };
                    }

                    this.Logger.WriteLine(call.Status);
                }

                broadcaster.SendToChannel("IPC_AddinToPopper", msg.Serialize());

                //this.msmq_IPC_AddinToPopper.Send(msg);
            }
            catch (Exception ex)
            {
                this.Logger.WriteLine("---------------------------------------------");
                this.Logger.WriteLine("Error: ");
                this.Logger.WriteLine(ex.Message);
                this.Logger.WriteLine(ex.StackTrace);
                this.Logger.WriteLine("---------------------------------------------");
            }
            this.Logger.WriteLine("_callCollection_ItemChanged - End");
        }

        private CallItem FindCallByCallID(string CallID)
        {
            foreach (var callsFolder in this._session.AllFolders(ItemType.CallItem))
            {
                foreach (CallItem call in callsFolder.Items())
                {
                    if (call.Id.ToString() == CallID)
                    {
                        return call;
                    }
                }
            }
            return null;
        }

        private void listener_MessageReceived(object sender, XDMessageEventArgs e)
        {
            try
            {
                IPC_PopperToAddin _msg = e.DataGram.Message.Deserialize<IPC_PopperToAddin>();
                if (_msg.CommandName == "Answer")
                {
                    var Call = this.FindCallByCallID(_msg.CallID);
                    if (Call != null && Call.Features.AllowAnswer) Call.Answer();
                }
                else if (_msg.CommandName == "Disconnect")
                {
                    var Call = this.FindCallByCallID(_msg.CallID);
                    if (Call != null && Call.Features.AllowDisconnect) Call.Disconnect();
                }
                else if (_msg.CommandName == "Dial")
                {
                    // create an address to call
                    Address address = _session.NewAddress(_msg.CommandArgument, AddressType.PhoneNumber);
                    address.UseRules = true;

                    // call the address
                    NewCallResult result = _session.NewCall(address);
                    if (result.NewCall != null)
                    {
                        if (_msg.CustomData != null && _msg.CustomData.Count > 0)
                        {
                            result.NewCall.Comments = String.Join(";", _msg.CustomData.Select(p => String.Format("{0}={1}", p.Name, p.Value)).ToArray());
                        }
                    }
                }
                else if (_msg.CommandName == "Show Log File")
                {
                    System.Diagnostics.Process.Start(this.LogPath);
                }
            }
            catch (Exception ex)
            {
                this.Logger.WriteLine("---------------------------------------------");
                this.Logger.WriteLine("Error: ");
                this.Logger.WriteLine(ex.Message);
                this.Logger.WriteLine(ex.StackTrace);
                this.Logger.WriteLine("---------------------------------------------");
            }
        }
    }
}