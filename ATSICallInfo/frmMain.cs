﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ATSI.ViewPoint.AddIns.Contract;
using ATSIViewpointAddins.Contract;
using TheCodeKing.Net.Messaging;

namespace ATSICallInfo
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();

            // Create our listener instance
            listener = XDListener.CreateListener(XDTransportMode.WindowsMessaging);

            // Register channels to listen on
            listener.RegisterChannel("IPC_AddinToPopper");
            listener.MessageReceived += new XDListener.XDMessageHandler(listener_MessageReceived);

            broadcaster = TheCodeKing.Net.Messaging.XDBroadcast.CreateBroadcast(XDTransportMode.WindowsMessaging, false);
        }

        public bool blAllowClose { get; set; }

        internal void ProcessCommand(string[] args)
        {
            var Arg = args.FirstOrDefault(p => p.ToLower().StartsWith("atsiphone"));
            if (!String.IsNullOrEmpty(Arg))
            {
                var myUri = new Uri(Arg);
                var Query = System.Web.HttpUtility.ParseQueryString(myUri.Query);
                if (!String.IsNullOrEmpty(Query["Command"]))
                {
                    switch (Query["Command"].ToLower())
                    {
                        case "dialphone":
                            if (!String.IsNullOrEmpty(Query["NumberToDial"]))
                            {
                                var NumberToDial = Query["NumberToDial"];
                                var msg = new IPC_PopperToAddin()
                                {
                                    CommandName = "Dial",
                                    CommandArgument = NumberToDial,
                                    CustomData = new List<CustomDataEntry>()
                                };
                                var SkipThese = new List<string>() { "command", "numbertodial" };
                                foreach (var key in Query.AllKeys)
                                {
                                    if (!SkipThese.Contains(key.ToLower()))
                                    {
                                        msg.CustomData.Add(new CustomDataEntry() { Name = key, Value = Query[key] });
                                    }
                                }

                                var DialNumber = false;
                                if (ATSICallInfo.Properties.Settings.Default.AutoDial)
                                {
                                    DialNumber = ATSICallInfo.Properties.Settings.Default.AutoDial;
                                }
                                else
                                {
                                    DialNumber = (MessageBox.Show(new Form() { TopMost = true }, String.Format("Are you sure you wish to dial: {0}?", NumberToDial), "Confirm", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes);
                                }

                                if (DialNumber)
                                {
                                    broadcaster.SendToChannel("IPC_PopperToAddin", msg.Serialize());
                                }
                            }
                            break;
                        case "playrecording":

                            break;
                    }
                }
            }
        }

        private string ActiveCallID = "";
        private IXDBroadcast broadcaster;
        private IXDListener listener;

        private void btnAnswer_Click(object sender, EventArgs e)
        {
            broadcaster.SendToChannel("IPC_PopperToAddin", new IPC_PopperToAddin()
            {
                CallID = this.ActiveCallID,
                CommandName = "Answer"
            }.Serialize());
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you wish to disconnect this call?", "Confirm", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                broadcaster.SendToChannel("IPC_PopperToAddin", new IPC_PopperToAddin()
                {
                    CallID = this.ActiveCallID,
                    CommandName = "Disconnect"
                }.Serialize());
            }
        }

        private void btnOpenRecord_Click(object sender, EventArgs e)
        {
            var URL = this.btnOpenRecord.Tag.ToString();
            if (!String.IsNullOrEmpty(URL))
            {
                System.Diagnostics.Process.Start(URL);
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            SetPosition();

            this.Visible = false;
        }

        private void SetPosition()
        {
            var RightMost = Screen.AllScreens.First(p => p.WorkingArea.Right == (Screen.AllScreens.Max(g => g.WorkingArea.Right)));
            Rectangle workingArea = RightMost.WorkingArea;
            this.Location = new Point(workingArea.Right - Size.Width, workingArea.Bottom - Size.Height);
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            switch (e.CloseReason)
            {
                case CloseReason.TaskManagerClosing:
                case CloseReason.WindowsShutDown:
                    this.blAllowClose = true;
                    break;
            }
            if (!this.blAllowClose)
            {
                e.Cancel = true;
                this.Visible = false;
            }
        }

        private void HandleNewMessage(XDMessageEventArgs e)
        {
            IPC_AddinToPopper _msg = e.DataGram.Message.Deserialize<IPC_AddinToPopper>();

            List<String> AcceptTheseAsNewCalls = new List<string>() { "Active", "Ringing", "Alerting" };

            if (String.IsNullOrEmpty(this.ActiveCallID) && AcceptTheseAsNewCalls.Contains(_msg.Status))
                this.ActiveCallID = _msg.CallID;

            if (this.ActiveCallID != _msg.CallID && _msg.Status == "Active") this.ActiveCallID = _msg.CallID;

            if (this.ActiveCallID == _msg.CallID)
            {
                if (_msg.NotificationType == "ItemDeleted" || _msg.Status == "Disconnected")
                {
                    this.Visible = false;
                    this.ActiveCallID = String.Empty;
                }
                else
                {
                    SetPosition();
                    this.Visible = true;
                    this.lblCallerIDName.Text = _msg.Info.CallerIdName ?? "";
                    this.lblCallerIDNumber.Text = _msg.Info.CallerIdNumber ?? "";

                    this.btnAnswer.Enabled = _msg.Info.AllowAnswer;
                    this.btnDisconnect.Enabled = _msg.Info.AllowDisconnect;

                    if (_msg.Info.CustomData != null && _msg.Info.CustomData.Count > 0)
                    {
                        var URL = "";
                        var MatchType = "";
                        var MatchedRecord = "";
                        if (_msg.Info.CustomData.Count(p => p.Name == "MatchType") == 1)
                            MatchType = _msg.Info.CustomData.First(p => p.Name == "MatchType").Value;

                        if (_msg.Info.CustomData.Count(p => p.Name == "MatchedRecord") == 1)
                            MatchedRecord = _msg.Info.CustomData.First(p => p.Name == "MatchedRecord").Value;

                        if (!String.IsNullOrEmpty(MatchType) && !String.IsNullOrEmpty(MatchedRecord))
                        {
                            switch (MatchType)
                            {
                                case "Shipment":
                                case "ShipmentContact":
                                    URL = "http://amps/Gateway.aspx?ObjectType=Shipment&ObjectID=" + MatchedRecord;
                                    break;

                                case "Supplier":
                                case "SupplierContact":
                                    URL = "http://amps/Gateway.aspx?ObjectType=Supplier&ObjectID=" + MatchedRecord;
                                    break;
                            }
                        }
                        if (!String.IsNullOrEmpty(URL))
                        {
                            this.btnOpenRecord.Visible = true;
                            this.btnOpenRecord.Tag = URL;
                        }
                        else
                        {
                            this.btnOpenRecord.Tag = "";
                            this.btnOpenRecord.Visible = false;
                        }
                        this.txtCustomData.Text = String.Join(Environment.NewLine, _msg.Info.CustomData.Select(p => p.Name + ": " + p.Value).ToArray());
                    }
                    else
                    {
                        this.txtCustomData.Text = "";
                        this.btnOpenRecord.Tag = "";
                        this.btnOpenRecord.Visible = false;
                    }
                }
            }
        }

        private void listener_MessageReceived(object sender, XDMessageEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() => HandleNewMessage(e)));
            }
            else
            {
                HandleNewMessage(e);
            }
        }

        private void mnuQuit_Click(object sender, EventArgs e)
        {
            this.blAllowClose = true;
            this.Close();
        }

        private void mnuShowLog_Click(object sender, EventArgs e)
        {

            broadcaster.SendToChannel("IPC_PopperToAddin", new IPC_PopperToAddin()
            {
                CommandName = "Show Log File"
            }.Serialize());
        }
    }
}