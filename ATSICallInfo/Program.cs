﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;

namespace ATSICallInfo
{
    static class Program 
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SingleInstanceController controller = new SingleInstanceController();
            string[] args = Environment.GetCommandLineArgs();
            controller.Run(args);

        }
        public class SingleInstanceController : WindowsFormsApplicationBase
        {
            public SingleInstanceController()
            {
                IsSingleInstance = true;

                StartupNextInstance += this_StartupNextInstance;
            }

            void this_StartupNextInstance(object sender, StartupNextInstanceEventArgs e)
            {
                frmMain form = MainForm as frmMain; //My derived form type
                form.ProcessCommand(e.CommandLine.ToArray());
            }

            protected override void OnCreateMainForm()
            {
                MainForm = new frmMain();
            }
        }
    }
}
