﻿namespace ATSICallInfo
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lblCallerIDName = new System.Windows.Forms.Label();
            this.lblCallerIDNumber = new System.Windows.Forms.Label();
            this.niCallInfo = new System.Windows.Forms.NotifyIcon(this.components);
            this.conNotifyIcon = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAnswer = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.txtCustomData = new System.Windows.Forms.TextBox();
            this.btnOpenRecord = new System.Windows.Forms.Button();
            this.mnuShowLog = new System.Windows.Forms.ToolStripMenuItem();
            this.conNotifyIcon.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCallerIDName
            // 
            this.lblCallerIDName.AutoSize = true;
            this.lblCallerIDName.Location = new System.Drawing.Point(13, 13);
            this.lblCallerIDName.Name = "lblCallerIDName";
            this.lblCallerIDName.Size = new System.Drawing.Size(0, 13);
            this.lblCallerIDName.TabIndex = 0;
            // 
            // lblCallerIDNumber
            // 
            this.lblCallerIDNumber.AutoSize = true;
            this.lblCallerIDNumber.Location = new System.Drawing.Point(13, 30);
            this.lblCallerIDNumber.Name = "lblCallerIDNumber";
            this.lblCallerIDNumber.Size = new System.Drawing.Size(0, 13);
            this.lblCallerIDNumber.TabIndex = 1;
            // 
            // niCallInfo
            // 
            this.niCallInfo.ContextMenuStrip = this.conNotifyIcon;
            this.niCallInfo.Icon = ((System.Drawing.Icon)(resources.GetObject("niCallInfo.Icon")));
            this.niCallInfo.Text = "ATSI Call Info";
            this.niCallInfo.Visible = true;
            // 
            // conNotifyIcon
            // 
            this.conNotifyIcon.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuShowLog,
            this.mnuQuit});
            this.conNotifyIcon.Name = "conNotifyIcon";
            this.conNotifyIcon.Size = new System.Drawing.Size(162, 70);
            // 
            // mnuQuit
            // 
            this.mnuQuit.Name = "mnuQuit";
            this.mnuQuit.Size = new System.Drawing.Size(161, 22);
            this.mnuQuit.Text = "&Quit";
            this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
            // 
            // btnAnswer
            // 
            this.btnAnswer.Enabled = false;
            this.btnAnswer.Location = new System.Drawing.Point(361, 3);
            this.btnAnswer.Name = "btnAnswer";
            this.btnAnswer.Size = new System.Drawing.Size(97, 23);
            this.btnAnswer.TabIndex = 7;
            this.btnAnswer.Text = "Answer";
            this.btnAnswer.UseVisualStyleBackColor = true;
            this.btnAnswer.Click += new System.EventHandler(this.btnAnswer_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Enabled = false;
            this.btnDisconnect.Location = new System.Drawing.Point(361, 101);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(97, 23);
            this.btnDisconnect.TabIndex = 8;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // txtCustomData
            // 
            this.txtCustomData.BackColor = System.Drawing.SystemColors.Control;
            this.txtCustomData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCustomData.Location = new System.Drawing.Point(13, 47);
            this.txtCustomData.Multiline = true;
            this.txtCustomData.Name = "txtCustomData";
            this.txtCustomData.Size = new System.Drawing.Size(342, 77);
            this.txtCustomData.TabIndex = 9;
            // 
            // btnOpenRecord
            // 
            this.btnOpenRecord.Location = new System.Drawing.Point(361, 72);
            this.btnOpenRecord.Name = "btnOpenRecord";
            this.btnOpenRecord.Size = new System.Drawing.Size(97, 23);
            this.btnOpenRecord.TabIndex = 10;
            this.btnOpenRecord.Text = "Open Record";
            this.btnOpenRecord.UseVisualStyleBackColor = true;
            this.btnOpenRecord.Visible = false;
            this.btnOpenRecord.Click += new System.EventHandler(this.btnOpenRecord_Click);
            // 
            // mnuShowLog
            // 
            this.mnuShowLog.Name = "mnuShowLog";
            this.mnuShowLog.Size = new System.Drawing.Size(161, 22);
            this.mnuShowLog.Text = "Show Addin &Log";
            this.mnuShowLog.Click += new System.EventHandler(this.mnuShowLog_Click);
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnAnswer;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 136);
            this.Controls.Add(this.btnOpenRecord);
            this.Controls.Add(this.txtCustomData);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.btnAnswer);
            this.Controls.Add(this.lblCallerIDNumber);
            this.Controls.Add(this.lblCallerIDName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.Text = "ATSI Call Info";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.conNotifyIcon.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCallerIDName;
        private System.Windows.Forms.Label lblCallerIDNumber;
        private System.Windows.Forms.NotifyIcon niCallInfo;
        private System.Windows.Forms.Button btnAnswer;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.TextBox txtCustomData;
        private System.Windows.Forms.Button btnOpenRecord;
        private System.Windows.Forms.ContextMenuStrip conNotifyIcon;
        private System.Windows.Forms.ToolStripMenuItem mnuQuit;
        private System.Windows.Forms.ToolStripMenuItem mnuShowLog;
    }
}

